Follow the kubernetes-ingress [installation instructions](https://github.com/nginxinc/kubernetes-ingress/blob/master/docs/installation.md)

Create namespace:  
`kubectl apply -f namespace.yaml`

Create service account:  
`kubectl apply -f service-account.yaml`

Get CA certificate:  
`kubectl get secrets -n openwow`  
`kubectl get secret -n openwow openwow-service-account-token-##### -o jsonpath="{['data']['ca\.crt']}"`

Get _decoded_ access token:  
`kubectl get secret -n openwow openwow-service-account-token-##### -o jsonpath="{['data']['token']}" | base64 -d`


Set CI/CD variables:  
`KUBE_CA_DATA` => CA certificate  
`KUBE_TOKEN` => access token  
`KUBE_URL` => Kubernetes endpoint, clusters>cluster>server in your config file

Copy secret.yaml.tmpl to secret.yaml. Set `postgres` and `redis` to base64-encoded connection strings:

postgres:  
`echo "Host=127.0.0.1; Database=openwow_site; Username=openwow; Password=topsecret;" | base64 -w 9999`

redis:  
`echo "127.0.0.1:6379,defaultDatabase=4,name=openwow_site" | base64 -w 9999`

Apply secret.yaml:  
`kubectl apply -f secret.yaml`

Apply ingress things:  
`kubectl apply -f ingress -R`
